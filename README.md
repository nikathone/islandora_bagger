# Islandora Bagger docker

Containerizing the [Islandora bagger](https://github.com/mjordan/islandora_bagger)
microservice using docker.

## Requirements

- [Git](https://git-scm.com/downloads)
- [Docker](https://www.docker.com/get-started)
- [Docker-compose](https://docs.docker.com/compose/install/) (optional)
- [Vagrant](https://www.vagrantup.com/downloads.html)
- [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

## Getting started

### Installing and Configuring Islandora 8 RDM flavor Locally

1. Follow the instructions found on the [RDM Playbook](https://github.com/roblib/rdm-playbook)
   to get islandora 8 RDM flavor running locally.
2. Download and install the [Islandora bagger integration](https://github.com/roblib/islandora_bagger_integration).
3. SSH into vagrant `vagrant ssh`
4. Create and set drupal private files directory.
5. Edit and place the [sample configuration](./sample_config.yml) under `<private-file-directory>/bagger_config/islandora_bagger_config.yml`
6. Navigate to the [Islandora bagger integration settings](http://localhost:8000/admin/config/islandora_bagger_integration/settings) page
7. For _Islandora bagger location_ select `Remote`
8. For _Path to default Islandora Bagger microservice config file. Normally starts with private://_ enter `private://bagger_config/islandora_bagger_config.yml`
9. For _Islandora Bagger microservice REST endpoint_ enter `10.0.2.2:8001/api/createbag`

### Building and running the containerized Islandora bagger

1. Create and navigate into a folder named `bagger` for example
2. Clone the [Islandora bagger](https://github.com/mjordan/islandora_bagger) repo into this folder. `git clone git@github.com:mjordan/islandora_bagger.git code`
3. Clone this repository under a folder named `docker`
4. Copy the `.dockerignore` file from `docker/.dockerignore` into the current repository. `cp docker/.dockerignore .`
5. Build the docker image containing the Islandora bagger code by running
   command below. If you plan to debug the bagger container based on this image,
   it might be good to change the `--target` flag and `--build-arg build_environment`
   value to `dev`.

```bash
docker image build --tag islandora_bagger:latest \
--file docker/Dockerfile \
--build-arg code_dir=./code \
--build-arg build_environment=prod \
--target prod
./
```

6. Run the container of the image build above with:

```bash
docker container run --interactive --tty \
--port 8001:8001 \
--env APP_SECRET="mysupersecretpassword" \
--name islandora_bagger \
islandora_bagger:latest \
--detach
```

7. If you are on an Operating system which is not MacOS or Windows, you will need to add the `--network host` flag and adjust the sample config accordingly.

#### Bind mounting the private folder

Currently the sample config has `output_dir` set to `/mnt/files/private`. This
folder can be bind mounted to the container host and give access to other
system to have access to the generated bag. Running the container with the
following flag `--volume <path-to-local-folder>:/mnt/files/private` will bind
mount that directory to the host.

### Creating bags

Go to the running local Islandora website and click `Create bag` on one of the repository item.

### Processing the queue

From the host machine run:

```bash
docker container exec --interactive --tty islandora_bagger \
php bin/console app:islandora_bagger:process_queue \
--queue=var/islandora_bagger.queue
```

## Developing Islandora Bagger

In a containerized environment can be achieve by using the [`docker-compose.yml`](./docker-compose.yml)
provided in this this repo.

Once the development is done a docker image can be built and pushed to a
remote docker registry.

## Running the container on remote server

Ideally the image should be built using a continuous integration system which
can package the Islandora bagger code into the image and push into a remote
registry. Then the image could be used on the remote server to run the container.

## Current limitation

### Access control/Authentication

This image doesn't provide any access control for the API but since it's
running nginx as it's webserver we can extend it's configuration to include a
[basic auth](https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-http-basic-authentication/)
or [JWT](https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-jwt-authentication/) Authentication.

### Outgoing email

The configuration of any outgoing email should be taken care of at the app level.

### Access to the generated bags

See https://github.com/mjordan/islandora_bagger/issues/17

### Logging

The app doesn't have a setup to log it's warning and errors to stdout. Looking
into https://symfony.com/doc/current/logging.html might be a good idea.
