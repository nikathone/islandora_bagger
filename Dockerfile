ARG code_dir=./code
ARG build_environment=prod
ARG php_nginx_base_img_version=7.2.28-1.17.8-10676096

#
# Stage 1: PHP Dependencies
#
FROM composer:1.9.3 as vendor
ARG code_dir
ARG build_environment
ARG COMPOSER_INSTALL_FLAGS="--ansi \
  --no-scripts \
  --no-plugins \
  --no-suggest \
  --prefer-dist \
  --no-interaction \
  --ignore-platform-reqs"

WORKDIR /app

COPY ${code_dir}/composer.json ${code_dir}/composer.lock ./

RUN set -eux; \
  flags="${COMPOSER_INSTALL_FLAGS}"; \
  # Exclude dev dependencies when build environment is prod.
  [ "$build_environment" == "prod" ] && flags="${COMPOSER_INSTALL_FLAGS} --no-dev"; \
  composer install $flags

#
# Stage 2: PHP Dependencies
#
FROM registry.gitlab.com/nikathone/drupal-docker-good-defaults/php-nginx:${php_nginx_base_img_version} as base
ARG code_dir
ENV APP_RUNNER_USER=root

# Docker entrypoint.
COPY ./docker/bin/docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh

# Make sure that the entrypoint is executable.
RUN   chmod +x /usr/local/bin/docker-entrypoint.sh; \
  # Prepare the configurations template for php-fpm.
  mkdir -p /etc/confd; \
  cp -R /confd_templates/* /etc/confd/; \
  # Apply custom configurations based on confd templates
  /usr/local/bin/confd -onetime -backend env

# Overwrite the main and default NGINX configurations.
COPY ./docker/config/nginx/nginx.conf ./docker/config/nginx/api_json_errors.conf /etc/nginx/
COPY ./docker/config/nginx/conf.d/api.conf /etc/nginx/conf.d/default.conf

EXPOSE 8001
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisor/conf.d/supervisord.conf"]

WORKDIR /var/www/app

# Getting the depencies from the composer/vendor stage.
COPY --from=vendor /app/vendor ./vendor
# Adding our current code.
COPY ${code_dir}/ ./

#
# Stage 3: The production setup
#
FROM base AS prod
ENV APP_ENV=prod

# Using the production php.ini
RUN mv ${PHP_INI_DIR}/php.ini-production ${PHP_INI_DIR}/php.ini

#
# Stage 4: The dev setup
#
FROM base AS dev

ARG PHP_XDEBUG
ARG PHP_XDEBUG_DEFAULT_ENABLE
ARG PHP_XDEBUG_REMOTE_CONNECT_BACK
ARG PHP_XDEBUG_REMOTE_HOST
ARG PHP_IDE_CONFIG

ENV APP_ENV=dev \
  DEBUG=true

# Install development tools.
RUN pecl install xdebug-2.7.1; \
  docker-php-ext-enable xdebug; \
  # Adding the dev php.ini
  mv ${PHP_INI_DIR}/php.ini-development ${PHP_INI_DIR}/php.ini; \
  # Copy xdebug configurations templates.
  cp /confd_templates/conf.d/docker-php-ext-xdebug.ini.toml /etc/confd/conf.d/docker-php-ext-xdebug.ini.toml; \
  cp /confd_templates/templates/docker-php-ext-xdebug.ini.tmpl /etc/confd/templates/docker-php-ext-xdebug.ini.tmpl; \
  # Apply xdebug configurations.
  /usr/local/bin/confd -onetime -backend env \
  # Delete xdebug configuration template files.
  && rm /etc/confd/conf.d/docker-php-ext-xdebug.ini.toml /etc/confd/templates/docker-php-ext-xdebug.ini.tmpl \
  # Remove the confd templates altogether.
  && rm -rf /confd_templates

# Copy composer binary from official Composer image for development purposes
COPY --from=composer:1.9.3 /usr/bin/composer /usr/bin/composer
